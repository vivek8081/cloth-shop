//@ts-check

const { createServe } = require("http")

const app = createServer((req, res) => {
    if (req.url === "/" && req.method === "GET") {
        res.write("Hello from root of the website")
        res.end()
    } else {
        res.write("page not found")
        res.end()
    }
})

module.exports = { app }
